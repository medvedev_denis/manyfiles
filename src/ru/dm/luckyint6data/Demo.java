package ru.dm.luckyint6data;


import java.io.*;


public class Demo {


    public static void main(String[] args) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream("int6Data.dat"));
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("txt6data.dat"));
        boolean cycleExit = false;
        int dataNum;
        int basicNum = 0;
        int intermedNum;
        int intFirst3Sum = 0;
        int intSecond3Sum = 0;
        while (!cycleExit){
            try {
                dataNum = dataInputStream.readInt();
                intermedNum = dataNum;
                basicNum = dataNum / 10;
                basicNum = intermedNum - basicNum * 10;
                intermedNum = (dataNum - basicNum)/10;
                intSecond3Sum = intSecond3Sum + basicNum;
                for (int i = 0;i < 2; i++){
                    basicNum = intermedNum / 10;
                    basicNum = intermedNum - basicNum * 10;
                    intermedNum = (intermedNum - basicNum)/10;
                    intSecond3Sum = intSecond3Sum + basicNum;
                }
                for (int i = 0;i < 3; i++){
                    basicNum = intermedNum / 10;
                    basicNum = intermedNum - basicNum * 10;
                    intermedNum = (intermedNum - basicNum)/10;
                    intFirst3Sum = intFirst3Sum + basicNum;
                }
                if (intFirst3Sum == intSecond3Sum)
                    dataOutputStream.writeInt(dataNum);
                intFirst3Sum = 0;
                intSecond3Sum = 0;
            } catch (EOFException e) {
                cycleExit = true;
            }
        }
        System.out.println("Процесс выполнен успешно");
        dataInputStream.close();
        dataOutputStream.close();
    }
}
