package ru.dm.intdata;


import java.io.*;
import java.util.Random;

public class Demo {
    static Random random = new Random();

    public static void main(String[] args) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("intData.dat"));
        int amountOfNumbers = random.nextInt(1000) + 1;
        for (int i = 0;i<amountOfNumbers; i++){
            dataOutputStream.writeInt(random.nextInt(10000000));
        }
        System.out.println("Процесс выполнен успешно");
        dataOutputStream.close();
    }
}
