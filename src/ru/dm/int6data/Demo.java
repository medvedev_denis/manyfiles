package ru.dm.int6data;


import java.io.*;

public class Demo {


    public static void main(String[] args) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream("intData.dat"));
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("int6Data.dat"));
        boolean cycleExit = false;
        int dataNum;
        while (!cycleExit){
            try {
                dataNum = dataInputStream.readInt();
                if (dataNum > 0 && dataNum < 1000000)
                    dataOutputStream.writeInt(dataNum);
            } catch (EOFException e){
                cycleExit = true;
            }
        }
        System.out.println("Процесс выполнен успешно");
        dataOutputStream.close();
        dataInputStream.close();
    }
}
